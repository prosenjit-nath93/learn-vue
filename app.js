new Vue({
    el: "#vue-app",
    data: {
        name: "Prosenjit Nath",
        website: "https://google.com",
        age: 20,
        x: 0,
        y: 0,
        a: 20,
        b: 10,
        if_condition: true,
        characters: ['sanu','sonu','ananya','tinku'],
        team: [
            {name: 'Sudipta', age: 22},
            {name: 'Mainak', age: 27},
            {name: 'Aditi', age: 27},
            {name: 'Monojit', age: 26},
            {name: 'Bony', age: 35}

        ]

    },
    methods: {
        greet: function(time) {
            return this.name+' Good '+time;
        },

        movemouse: function(event) {
            this.x= event.offsetX;
            this.y= event.offsetY
        },
        click: function() {
            alert('click me');
        },
        log: function() {
            console.log("console log");
        }
    },
    computed: {
    
        diff: function() {
            console.log('difference method called');
            return this.a - this.b;
        }
    }
})